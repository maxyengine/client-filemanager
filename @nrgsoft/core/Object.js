import Value from '@nrgsoft/core/Value'
import Trait from '@nrgsoft/core/Trait'

export default class extends Value {

  _constructor (...args) {
    this.use(...this.traits || [])
    super._constructor(...args)
  }

  use (...definitions) {
    definitions.forEach(definition => Trait.use(this, definition))

    return this
  }
}
