const ClassBuilder = require('./ClassBuilder')

module.exports = function (xml) {
  this.callback(null, new ClassBuilder().build(xml).toString())
}
