import Component from '@nrgsoft/core/Component'
import Widget from '@nrgsoft/ui/Widget'
import Element from '@nrgsoft/ui/Element'
import Definition from '@nrgsoft/core/Definition'

const wrapper = Symbol()
const layouts = Symbol()
const current = Symbol()

export default class extends Component {

  get defaults () {
    return {
      minHeight: 500
    }
  }

  get wrapper () {
    return this[wrapper]
  }

  set wrapper (element) {
    let wrapElement = element

    if (document.body !== element) {
      const clientHeight = element.clientHeight || 0

      wrapElement = Element.insert(element, document.createElement('div'))
      wrapElement.style.position = 'relative'
      wrapElement.style.height = clientHeight >= this.minHeight ? clientHeight + 'px' : this.minHeight + 'px'
    }

    this[wrapper] = new Widget(wrapElement)
  }

  _constructor (...args) {
    this[layouts] = new Map()

    super._constructor(...args)
  }

  getLayout (def) {
    const
      definition = new Definition(def),
      className = definition.className

    if (!this[layouts].has(className)) {
      this[layouts].set(className, definition)
    }

    const layout = this[layouts].get(className).getInstance(this.injector)

    if (this[current] !== className) {
      this[current] = className
      layout.show(this[wrapper])
    }

    return layout
  }
}