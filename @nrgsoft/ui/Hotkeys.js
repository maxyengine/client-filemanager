import Value from '@nrgsoft/core/Value'
import Hotkey from '@nrgsoft/ui/Hotkey'

const hotkeys = Symbol()

export default class extends Value {

  get assignments () {
    return [
      'setHotkey'
    ]
  }

  initialize () {
    this[hotkeys] = new Map()

    this.setHotkeys(this.owner.hotkeys || {})

    document.addEventListener('keydown', event => {
      if (!this.owner.isActiveHotkeys) {
        return
      }

      for (const [eventName, hotkey] of this[hotkeys]) {
        if (hotkey.isRecognized(event)) {
          event.preventDefault()

          if (!event.stopTrigger) {
            this.owner.trigger(eventName, event)
          }

          return
        }
      }
    })
  }

  setHotkeys (hotkeys) {
    for (const [eventName, hotkey] of Object.entries(hotkeys)) {
      this.setHotkey(eventName, hotkey)
    }
  }

  setHotkey (eventName, hotkey) {
    this[hotkeys].set(eventName, new Hotkey(hotkey))
  }
}
