import List from '@nrgsoft/data/List'
import Element from '@nrgsoft/ui/Element'

const container = Symbol()

export default class extends List {

  get [container] () {
    return this.listContainer || this
  }

  onClear () {
    Element.clear(this[container])
  }

  onAddItem ({item}) {
    Element.append(this[container], item)
  }

  onAppendItem ({child}) {
    this.appendItem(child)
  }

  onRemoveItem ({item}) {
    Element.remove(this[container], item)
  }

  onBeforeCreateItem ({services}) {
    services.t = this.t
  }
}
