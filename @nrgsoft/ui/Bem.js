import Value from '@nrgsoft/core/Value'

const bemBlock = Symbol()
const bemElements = Symbol()
const createBemElement = Symbol()
const createBemModifier = Symbol()

export default class extends Value {

  get assignments () {
    return [
      'setBemBlock',
      'addBemElement',
      'addBemModifier',
      'removeBemModifier',
      'toggleBemModifier'
    ]
  }

  get element () {
    return this.owner.element
  }

  initialize () {
    this[bemElements] = new Map()
  }

  setBemBlock (className) {
    if (this[bemBlock]) {
      throw new Error('BEM block is already defined')
    }

    this.element.classList.add(className)
    this[bemBlock] = className

    return this.owner
  }

  addBemElement (domElement, className) {
    if (this.element === domElement) {
      throw new Error('Attempt to make the root element as a BEM element')
    }

    if (this[bemElements].has(domElement)) {
      throw new Error('BEM element is already defined')
    }

    domElement.classList.add(this[createBemElement](domElement, className))

    return this.owner
  }

  addBemModifier (domElement, className) {
    domElement.classList.add(this[createBemModifier](domElement, className))

    return this.owner
  }

  removeBemModifier (domElement, className) {
    domElement.classList.remove(this[createBemModifier](domElement, className))

    return this.owner
  }

  toggleBemModifier (domElement, className) {
    domElement.classList.toggle(this[createBemModifier](domElement, className))

    return this.owner
  }

  [createBemElement] (domElement, className) {
    const bemClassName = `${this[bemBlock] || ''}__${className}`
    this[bemElements].set(domElement, bemClassName)

    return bemClassName
  }

  [createBemModifier] (domElement, className) {
    return this.element === domElement ?
      `${this[bemBlock] || ''}--${className}` :
      `${this[bemElements].get(domElement) || ''}--${className}`
  }
}