const self = class {

  static append (to, what) {
    const
      toElement = self.getElement(to),
      whatElement = self.getElement(what)

    toElement.appendChild(whatElement)

    return what
  }

  static appendBefore (to, what, before) {
    const
      toElement = self.getElement(to),
      whatElement = self.getElement(what),
      beforeElement = self.getElement(before)

    toElement.insertBefore(whatElement, beforeElement)

    return what
  }

  static insert (to, what) {
    self.clear(to)

    return self.append(to, what)
  }

  static remove (from, what) {
    const
      fromElement = self.getElement(from),
      whatElement = self.getElement(what)

    fromElement.removeChild(whatElement)

    return what
  }

  static clear (component) {
    const element = self.getElement(component)

    while (element.hasChildNodes()) {
      element.removeChild(element.firstChild)
    }

    return component
  }

  static contains (where, what) {
    const
      whereElement = self.getElement(where),
      whatElement = self.getElement(what)

    return whereElement.contains(whatElement)
  }

  static getElement (component) {
    if (!component) {
      return document.createTextNode('')
    }

    if (self.isNode(component)) {
      return component
    }

    return component.element || document.createTextNode(component)
  }

  static isNode (component) {
    return component &&
      typeof component === 'object' &&
      typeof component.nodeName === 'string' &&
      [1, 3, 8].includes(component.nodeType)

  }
}

export default self