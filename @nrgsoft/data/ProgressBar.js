import Component from '@nrgsoft/core/Component'

export default class extends Component {

  show () {
    this.classList.remove('d-none')
  }

  hide () {
    this.classList.add('d-none')
  }

  update (percent) {
    this.progress.style.width = percent + '%'
  }

  resetProgress () {
    this.update(0)
  }
}
