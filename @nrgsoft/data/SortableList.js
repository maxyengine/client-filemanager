import List from '@nrgsoft/data/List'

export default class extends List {

  static get services () {
    return {
      router: 'router'
    }
  }

  get defaults () {
    return {
      paramName: 'orderBy',
      literalAsc: 'asc',
      literalDesc: 'desc',
      beforeGoTo: 'beforeGoTo'
    }
  }

  get url () {
    return this.router.url
  }

  get orderBy () {
    const orderBy = this.url.getParam(this.paramName, [])

    return Array.isArray(orderBy) ? orderBy : Object.values(orderBy)
  }

  onBeforeCreateItem (event) {
    const properties = event.properties

    if (!properties.sortable) {
      return
    }

    properties.url = this.createUrl(properties.name)
    properties.direction = this.getDirection(properties.name)
  }

  onAddItem (event) {
    event.item.on(this.beforeGoTo, (event, column) => {
      event.link.url = this.createUrl(column.name, event.nativeEvent.shiftKey)
    })
  }

  createUrl (fieldName, shift) {
    return this.url.clone().mergeParams({
      orderBy: this.createOrderBy(fieldName, shift)
    })
  }

  createOrderBy (fieldName, shift) {
    const orderBy = [...this.orderBy]

    for (let i = 0; orderBy[i]; i++) {
      if (orderBy[i].field === fieldName) {
        switch (orderBy[i].direction) {
          case undefined:
            orderBy[i].direction = this.literalAsc
            break
          case this.literalAsc:
            orderBy[i].direction = this.literalDesc
            break
          case this.literalDesc:
            delete orderBy[i]
            break
        }

        return shift ? orderBy : (orderBy[i] ? [orderBy[i]] : [])
      }
    }

    const item = {
      field: fieldName,
      direction: this.literalAsc
    }

    if (shift) {
      orderBy.push(item)

      return orderBy
    }

    return [item]
  }

  getDirection (fieldName) {
    for (let i = 0; this.orderBy[i]; i++) {
      const orderBy = this.orderBy[i]
      if (orderBy.field === fieldName) {
        return orderBy.direction
      }
    }
  }
}