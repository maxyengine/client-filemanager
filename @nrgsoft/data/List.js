import Component from '@nrgsoft/core/Component'
import Definition from '@nrgsoft/core/Definition'

const data = Symbol()
const items = Symbol()

export default class extends Component {

  set data (data) {
    this.clear()

    data.forEach(properties => {
      this.addItem(properties)
    })

    this.trigger('setData')
  }

  get items () {
    return this[items]
  }

  get length () {
    return this[items].length
  }

  get isEmpty () {
    return !this.length
  }

  get lastIndex () {
    return this.length - 1
  }

  get lastItem () {
    return this.getItem(this.lastIndex)
  }

  _constructor (...args) {
    this[data] = []
    this[items] = []

    super._constructor(...args)
  }

  getRaw (index, byDefault) {
    return this[data][index] || byDefault
  }

  getItem (index, byDefault) {
    return this[items][index] || byDefault
  }

  getItemByRaw (raw) {
    return this.getItem(this[data].indexOf(raw))
  }

  addItem (raw = {}) {
    this[data].push(raw)

    const event = {
      item: this.createItem(raw),
      index: this.length
    }

    this[items].push(event.item)

    this.trigger('addItem', event)

    return event.item
  }

  appendItem (item) {
    this[data].push({})
    this[items].push(item)

    this.trigger('addItem', {
      item: item,
      index: this.lastIndex
    })

    return item
  }

  removeItem (item) {
    const index = this[items].indexOf(item)

    this[items].splice(index, 1)
    this[data].splice(index, 1)

    this.trigger('removeItem', {item: item})

    if (this.isEmpty) {
      this.clear()
    }

    return this
  }

  clear () {
    this.trigger('beforeClear')

    this[data] = []
    this[items] = []

    this.trigger('clear')

    return this
  }

  createItem (properties) {
    const event = {
      itemClass: this.itemClass,
      properties: properties,
      services: {}
    }

    this.trigger('beforeCreateItem', event)

    return new Definition(event.itemClass, event.properties, event.services).getInstance(this.injector)
  }

  [Symbol.iterator] () {
    return this[items].entries()
  }
}
