import _ from 'lodash'
import Obj from '@nrgsoft/utils/Object'

import containsDeep from 'object-contains-deep'

/*var o1 = {'a': 1, orderBy: [{field: 'name', direction: 'asc'}, {field: 'createdAt', direction: 'asc'}]}
var o2 = {'a': 1, orderBy: [{field: 'name', direction: 'asc'}, {field: 'createdAt', direction: 'asc'}]}*/

var o1 = {a: 1, b: {d: 99, v: 80}}
var o2 = {a: 1, b: {d: 99}}

test('is equal', () => {
  expect(containsDeep(o1, o2)).toBeTruthy()
})