export default class {

  appendTo (parent) {
    parent.appendChild(this.element)

    return this
  }

  constructor () {
    this.element = document.createElement('div')
    this.title = this.element.appendChild(document.createElement('h1'))
    this.content = this.element.appendChild(document.createElement('p'))

    this.title.innerHTML = 'Welcome Article'
    this.content.innerHTML = 'Bla bla bla bla bla bla bla bla bla bla bla bla bla'
  }
}