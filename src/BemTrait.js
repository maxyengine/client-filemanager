import Observer from '@nrgsoft/core/Observer'
import Element from '@nrgsoft/ui/Element'

export default class extends Observer {

  onAppendChild (event, widget) {
    Element.append(widget, event.child)
  }
}