import Observer from '@nrgsoft/core/Observer'
import Item from './Item.html'

export default class extends Observer {

  onAppendChild ({child}) {
    this.owner.appendItem(new Item({content: child}))
  }
}