import List from './List.html'
import Item from './Item.html'

const list = new List()
list.appendTo(document.body)

/*list.on('addItem', ({item}) => {
  item.on('click', () => console.log(item.element))
})*/

list
  .on('addItem', ({item}) => {
    list.mapEvent(item, 'click')
    list.mapEvent(item, 'hover')
  })
  .on('click', (event, list, item) => {
    console.log(item.content)
  })
  .on('hover', (event, list, item) => {
    console.log(item.element)
  })

list.data = [
  {content: 'One'},
  {content: 'Two'},
  {content: 'Three'}
]

//list.mapEvent(list.items, 'click')