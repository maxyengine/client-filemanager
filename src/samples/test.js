import Component from '@nrgsoft/core/Component'
import Element from '@nrgsoft/ui/Element'

const bemBlock = Symbol()
const bemElements = Symbol()
const createBemElement = Symbol()
const createBemModifier = Symbol()

const e1 = Symbol()
const e2 = Symbol()
const e3 = Symbol()
const e4 = Symbol()
const e5 = Symbol()
const e6 = Symbol()

export default class extends Component {

  get content () {
    return this[e3]
  }

  get events () {
    return {
      ...super.events || {},
      toggleStyle: {[e5]: 'click'}
    }
  }

  appendTo (parent) {
    return Element.append(parent, this)
  }

  setBemBlock (className) {
    if (!this[bemBlock]) {
      this.element.classList.add(className)
      this[bemBlock] = className
    }

    return this
  }

  addBemElement (domElement, className) {
    if (this.element === domElement) {
      throw new Error('Attempt to make the root element as a BEM element')
    }

    if (this[bemElements].has(domElement)) {
      throw new Error('BEM element is already defined')
    }

    domElement.classList.add(this[createBemElement](domElement, className))

    return this
  }

  addBemModifier (domElement, className) {
    domElement.classList.add(this[createBemModifier](domElement, className))

    return this
  }

  removeBemModifier (domElement, className) {
    domElement.classList.remove(this[createBemModifier](domElement, className))

    return this
  }

  toggleBemModifier (domElement, className) {
    domElement.classList.toggle(this[createBemModifier](domElement, className))

    return this
  }

  [createBemElement] (domElement, className) {
    const bemClassName = `${this[bemBlock] || ''}__${className}`
    this[bemElements].set(domElement, bemClassName)

    return bemClassName
  }

  [createBemModifier] (domElement, className) {
    return this.element === domElement ?
      `${this[bemBlock] || ''}--${className}` :
      `${this[bemElements].get(domElement) || ''}--${className}`
  }

  _constructor(properties) {
    this[bemElements] = new Map()

    this.element = document.createElement('div')
    this.setBemBlock('vendor-lib-Component')
    this.addBemModifier(this.element, 'bordered')
    this[e1] = document.createElement('h1')
    this.addBemElement(this[e1], 'title')
    this[e2] = document.createTextNode('Welcome Article')
    this[e1].appendChild(this[e2])
    this.element.appendChild(this[e1])
    this[e3] = document.createElement('p')
    this.addBemElement(this[e3], 'content')
    this[e4] = document.createTextNode('Bla bla bla bla bla bla')
    this[e3].appendChild(this[e4])
    this.element.appendChild(this[e3])
    this[e5] = document.createElement('button')
    this[e6] = document.createTextNode('Click me')
    this[e5].appendChild(this[e6])
    this.element.appendChild(this[e5])

    super._constructor(properties)


  }
}
