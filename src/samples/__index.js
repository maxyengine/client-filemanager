import Widget from '../Widget.html'

const w = new Widget({
  onAppendTitle: event => {
    console.log(event.element, event.element.innerHTML)
  },
  onAppendList: event => {
    console.log(event.element.items)
  }
})
const list = w.list

w.appendTo(document.body)

list
  .on('addItem', ({item}) => {
    list.mapEvent(item, 'click')
    list.mapEvent(item, 'hover')
  })
list.data = [
  {content: 'One'},
  {content: 'Two'},
  {content: 'Three'}
]

console.log(w.thisA, list.a)
console.log(w.thisB, list.b)

w.thisA = 1000
w.thisB = 2000

console.log(w.thisA, list.a)
console.log(w.thisB, list.b)

console.log(w.items.map(item => item.content))

w
  .on('select', () => {
    console.log('w.select')
    w.subList = document.createTextNode('TTTUYTUY')
  })
  .on('clickTitle', () => console.log('w.clickTitle'))

w.styles = [
  'background: #798554',
  'color: #ffffff'
].join(';')


/*
const xml = `
<div 
  import:List="./List.html"
  attr:style="styles"
>
	
	<h1
		mount:appendTitle
	> Hello {@title|NrgSoft} </h1>
	
	<List
		mount:appendList
		prop:list
		prop:thisA="a"
		prop:thisB="b"
		prop:items="items"
		event:select="click"
		event:clickTitle="title.click"
		a="100"
		b="200"
	>
	  <div style="background: blueviolet; color: #ffffff">
	    <h3>Child - 1</h3>
	  </div>
	  <div style="background: blueviolet; color: #ffffff">
	    <h3>Child - 2</h3>
	  </div>
  </List>
</div>
`
import ClassBuilder from '@nrgsoft/html-loader/ClassBuilder'

console.log(new ClassBuilder().build(xml).toString())
*/
