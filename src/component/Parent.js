import Component from '@nrgsoft/core/Component'

export default class extends Component {

  onToggleDesc (event, self) {
    self.showDesc = !self.showDesc
  }

  onSetShowDesc ({value}, self) {
    self.stateDesc = value ? 'Hide' : 'Show'
    console.log(self.stateDesc)
  }

  onToggleBlueHeader (event, self) {
    self.headerTheme = 'blue'
  }

  onToggleGreenHeader (event, self) {
    self.headerTheme = 'green'
  }

  onToggleOrangeHeader (event, self) {
    self.headerTheme = 'orange'
  }

  onSetHeaderTheme ({value}) {
    console.log(value)
  }
}